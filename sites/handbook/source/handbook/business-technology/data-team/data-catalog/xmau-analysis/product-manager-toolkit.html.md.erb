---
layout: handbook-page-toc
title: Product Manager Toolkit
---

## On this page
{:.no_toc}

- TOC
{:toc}

---

## Purpose of this page

As a Product Manager, you constantly have data-related questions. You want to know how your 
feature is performing, who your top users are, how many self-managed instances have 
adopted a specific feature, etc. Plus, you potentially want all of these metrics broken out 
by product tier, paid vs free, edition, etc.

While we have a lot of data, accessing some of it requires some level of proficiency 
and comfort with SQL. And even though you can try to copy some of the existing charts to tune 
them to answer the question you have, it can be time-consuming and you might not be 100% confident 
that the chart shows accurate data.

The Data program has created a set of different snippets to address your needs. This list is not 
exhaustive, and is mainly based on the feedback, questions, and tasks we received in the past 
from PMs. All of these snippets will be explained with detailed examples on how to change them 
and adapt them to your own needs. These are mainly focused on Self-Managed data from Service Ping.

If you prefer to learn by doing, we provide [a documented example Sisense dashboard](https://app.periscopedata.com/app/gitlab/793297/xMAU-Analysis-Workflow---Example-Queries-and-Visualisations) 
that you can check out, and most charts include documentation in their SQL edit view to help 
you customize them. You can look at the code used to generate the charts. The code has some 
extra guidelines commented out that will help you better understand how the given snippet works 
and what you can achieve with it.

Overall, these snippets are meant to help any team member who is looking for specific data from 
Service Ping. The goal of this page is to enable self-service analysis, in addition to building 
greater confidence in our data and models. We created [an issue (internal link)](https://gitlab.com/gitlab-data/analytics/-/issues/7738) 
to gather feedback. Please add any feedback on this page and the snippets provided.

The snippets listed below will allow you to:

- create a visualization with estimated Self-Managed uplift for a Service Ping metric (either 
an xMAU metric or other PI) (`[td_xmau]`)
- get a monthly count of instances using one of your features (`[td_xmau_metrics_instance_count]`)
- get a instance adoption rate of your feature (`[td_xmau_metrics_instance_adoption]`)
- get a list of your Top Users, Top Growing Users, or Churning Users (`[td_xmau_metrics_top_users]`, 
`[td_xmau_metrics_increasing_usage_users]`, `[td_xmau_metrics_decreasing_usage_users]`)
- gain more insights into a drop or an increase of your feature usage (`[td_xmau_monthly_change_breakdown]`)

## Trusted Data snippets

All these snippets start with a `td_` (Trusted Data) prefix and are considered to meet the 
requirements for the Data team's [Trusted Data Framework](/handbook/business-technology/data-team/platform/#tdf).

### Estimated xMAU or PI chart: [td_xmau(user_segment, xmau_type, filter, target, estimation_grain)]

The `[td_xmau]` snippet allows you to:

* [Create a standard xMAU chart](https://app.periscopedata.com/app/gitlab/793297/xMAU-Analysis-Workflow---Example-Queries-and-Visualisations?widget=10690032&udv=0) 
(what is embedded in the handbook)
* [Break down estimated xMAU by edition](https://app.periscopedata.com/app/gitlab/793297/xMAU-Analysis-Workflow---Example-Queries-and-Visualisations)
* [Break down estimated xMAU by product tier](https://app.periscopedata.com/app/gitlab/793297/xMAU-Analysis-Workflow---Example-Queries-and-Visualisations?widget=10690026&udv=0)
* [Create a chart with estimated usage for any PI](https://app.periscopedata.com/app/gitlab/793297/xMAU-Analysis-Workflow---Example-Queries-and-Visualisations?widget=10787341&udv=0)

**Usage**

The `[td_xmau]` snippet requires 5 parameters: `user_segment`, `xmau_type`, `filter`, `target`, 
and `estimation_grain`.

1. **user_segment:** 'All' or 'Paid'
1. **xmau_type:** one of the xMAU types ('GMAU', 'SMAU', 'CMAU', 'UMAU') or 'PI'
1. **filter:** the metric(s) you want included in the chart. This value is dependent on the xMAU 
type. The inputted value should be lowercase with underscores in the place of spaces.
  * 'GMAU' --> 'group_name' or 'All'
  * 'SMAU' --> 'stage_name' or 'All'
  * 'CMAU' --> 'section_name' or 'All'
  * 'UMAU' --> 'All'
  * 'PI' --> 'metrics_path' 
1. **target**: the target to draw for this metric 
    * If you set a target value in the performance indicator yaml file ([as described below](#how-to-show-dynamic-targets-from-pi-yaml-files)), 
    then that value will be used (you can enter any number here)
    * If you provide a number between 0 and 1, the target will increase MoM by that percentage. 
      * Ex: 0 will yield a target that equal to the previous month's total
      * Ex: 0.1 will yield a target that is a 10% increase from the previous month's total
    * If you provide an integer, that number will be the fixed target line
1. **estimation_grain:** The estimation methodology you want to use
  * 'metric/version check - subscription based estimation' (This is our "official" methodology and what was used in legacy reporting)
  * 'metric/version check - seat based estimation'
  * 'reported metric - subscription based estimation'
  * 'reported metric - seat based estimation'
  * More details about the different estimation_grains on the [Self-Managed Estimation Algorithm handbook page](/handbook/business-technology/data-team/data-catalog/xmau-analysis/estimation-xmau-algorithm.html#current-methodologies)

**Examples**

* [Paid Protect SMAU](https://app.periscopedata.com/app/gitlab/793297/xMAU-Analysis-Workflow---Example-Queries-and-Visualisations?widget=10690032&udv=0): `[td_xmau('Paid', 'SMAU', 'protect', 0.1, 'metric/version check - subscription based estimation')]`
* [Total Web IDE Editors](https://app.periscopedata.com/app/gitlab/793297/xMAU-Analysis-Workflow---Example-Queries-and-Visualisations?widget=10787341&udv=0): `[td_xmau('All', 'PI', 'usage_activity_by_stage_monthly.create.action_monthly_active_users_web_ide_edit', 0, 'metric/version check - seat based estimation')]`
* [Total Plan SMAU](https://app.periscopedata.com/app/gitlab/793297/xMAU-Analysis-Workflow---Example-Queries-and-Visualisations?widget=10690035&udv=0): `[td_xmau('All', 'SMAU', 'create', 0.1, 'metric/version check - subscription based estimation')]`
* [Total Dev CMAU](https://app.periscopedata.com/app/gitlab/793297/xMAU-Analysis-Workflow---Example-Queries-and-Visualisations?widget=10690026&udv=0): `[td_xmau('All', 'CMAU', 'dev', 5, 'metric/version check - subscription based estimation')]`

### Recorded Monthly Metric Value: [td_xmau_metrics_recorded_metric_value(metrics_path)]

The `[td_xmau_metrics_recorded_metric_value]` snippet shows you the recorded (non-estimated) 
value of any of your metrics implemented in Service Ping. For all-time metrics, this returns 
the installation-level change from the previous period.

The resulting dataset will enable you to break down recorded usage by:

* delivery (SaaS vs Self-Managed)
* edition
* product_tier

**Usage**

The `[td_xmau_metrics_recorded_metric_value]` snippet requires 1 parameter: `metrics_path`.

1. **metrics_path:** the Service Ping metric of interest
  * You can find the metrics_path value in the [Service Ping metrics dictionary](https://metrics.gitlab.com/)
   
You can add a target value to the chart by adding a `target_name` and a 
`monthly_estimated_targets` key to the performance indicator yaml [as described below](#how-to-show-dynamic-targets-from-pi-yaml-files).

If a target value is not provided in the performance indicator yaml file, a target will not 
appear on the chart.

**Example**

* [td_xmau_metrics_recorded_metric_value: Metric Value split by product tier](https://app.periscopedata.com/app/gitlab/793297/xMAU-Analysis-Workflow---Example-Queries-and-Visualisations?widget=10999660&udv=0): 
`[td_xmau_metrics_recorded_metric_value('redis_hll_counters.issues_edit.g_project_management_issue_moved_monthly')]`

### Recorded Monthly Metric Value Change with Total: [td_xmau_metrics_recorded_metric_value_with_total(metrics_path)]

This snippet extends `td_xmau_metrics_recorded_metric_value` with a total line.

**Usage**

The `[td_xmau_metrics_recorded_metric_value_with_total]` snippet requires 1 parameter: `metrics_path`.

1. **metrics_path:** the Service Ping metric of interest
  * You can find the metrics_path value in the [Service Ping metrics dictionary](https://metrics.gitlab.com/)

**Example**

* [td_xmau_metrics_recorded_metric_value_with_total: Metric Value split by edition](https://app.periscopedata.com/app/gitlab/793297/xMAU-Analysis-Workflow---Example-Queries-and-Visualisations?widget=14866036&udv=0): 
`[td_xmau_metrics_recorded_metric_value_with_total('redis_hll_counters.issues_edit.g_project_management_issue_moved_monthly')]`

### Original Monthly Metric Value: [td_xmau_metrics_original_metric_value_with_total(metrics_path)]

This snippet is a variation of `td_xmau_metrics_recorded_metric_value_with_total` that shows the 
original value (as it appears in the Service Ping payload), instead of a change from the previous 
period. For 28-day metrics, this will return the same results as the 
`td_xmau_metrics_recorded_metric_value_with_total` snippet.

**Usage**

The `[td_xmau_metrics_original_metric_value_with_total]` snippet requires 1 parameter: `metrics_path`.

1. **metrics_path:** the Service Ping metric of interest
  * You can find the metrics_path value in the [Service Ping metrics dictionary](https://metrics.gitlab.com/)

### Recorded Weekly Metric Value: [td_xmau_metrics_weekly_recorded_metric_value(metrics_path)]

The `[td_xmau_metrics_weekly_recorded_metric_value]` snippet shows you the recorded (non-estimated) 
value of any of your 7-day metrics implemented in Service Ping.

The resulting dataset will enable you to break down recorded usage by:

* delivery (SaaS vs Self-Managed)
* edition
* product_tier

**Usage**

The `[td_xmau_metrics_weekly_recorded_metric_value]` snippet requires 1 parameter: `metrics_path`.

1. **metrics_path:** the Service Ping metric of interest
  * You can find the metrics_path value in the [Service Ping metrics dictionary](https://metrics.gitlab.com/)

**Example**

* [td_xmau_metrics_weekly_recorded_metric_value: Metric Value split by product tier](https://app.periscopedata.com/app/gitlab/793297/xMAU-Analysis-Workflow---Example-Queries-and-Visualisations?widget=15240707&udv=0): 
`[td_xmau_metrics_weekly_recorded_metric_value('redis_hll_counters.user_packages.i_package_npm_user_weekly')]`

### Recorded Weekly Metric Value with Total: [td_xmau_metrics_weekly_recorded_metric_value_with_total(metrics_path)]

This snippet extends `td_xmau_metrics_weekly_recorded_metric_value` with a total line.

**Usage**

The `[td_xmau_metrics_weekly_recorded_metric_value_with_total]` snippet requires 1 parameter: `metrics_path`.

1. **metrics_path:** the Service Ping metric of interest
  * You can find the metrics_path value in the [Service Ping metrics dictionary](https://metrics.gitlab.com/)

**Example**

* [td_xmau_metrics_weekly_recorded_metric_value_with_total: Metric Value split by edition](https://app.periscopedata.com/app/gitlab/793297/xMAU-Analysis-Workflow---Example-Queries-and-Visualisations?widget=15240714&udv=0): 
`[td_xmau_metrics_weekly_recorded_metric_value_with_total('redis_hll_counters.user_packages.i_package_npm_user_weekly')]`

### Monthly Active Instance Count: [td_xmau_metrics_instance_count(metrics_path)]

The snippet `[td_xmau_metrics_instance_count]` shows you how many instances have used your 
feature (returned a `metric_value > 0`) in a given month. This count is broken out by:

* ping_delivery_type (always `'Self-Managed'` in this snippet)
* ping_edition
* ping_product_tier 

**Usage**

The `[td_xmau_metrics_instance_count]` snippet requires 1 parameter: `metrics_path`.

1. **metrics_path:** the Service Ping metric of interest
  * You can find the metrics_path value in the [Service Ping metrics dictionary](https://metrics.gitlab.com/)

**Example**

* [Number of Instances using CI Pipelines by Edition](https://app.periscopedata.com/app/gitlab/793297/xMAU-Analysis-Workflow---Example-Queries-and-Visualisations?widget=10755606&udv=0): `[td_xmau_metrics_instance_count('usage_activity_by_stage_monthly.verify.ci_pipelines')]`

### Monthly Metrics Instance Adoption: [td_xmau_metrics_instance_adoption(breakdown, metrics_path)]

The snippet `[td_xmau_metrics_instance_adoption]` can be used to find out instance-level 
adoption of your features. It provides you the count and share of instances reporting 
`metric_value > 0` for a specific Service Ping metric.

**Usage**

The `[td_xmau_metrics_instance_adoption]` snippet requires 2 parameters:

1. **breakdown:** How you want the data grouped. This value should _not_ be put in single 
quotes. (Ex: use `ping_edition`, not `'ping_edition'`). Here are some example breakdowns 
(non-exhaustive list):
  * ping_edition
  * ping_product_tier 
  * ping_edition_product_tier
  * major_minor_version
1. **metrics_path:** the Service Ping metric of interest. This value _should_ be put in single 
quotes. (Ex: use `'usage_activity_by_stage_monthly.release.deployments'`, not `usage_activity_by_stage_monthly.release.deployments`)

**Examples**

* [td_xmau_metrics_instance_adoption: Issues by ping_edition](https://app.periscopedata.com/app/gitlab/793297/xMAU-Analysis-Workflow---Example-Queries-and-Visualisations?widget=10766391&udv=0): `[td_xmau_metrics_instance_adoption(ping_edition, 'usage_activity_by_stage_monthly.plan.issues')]`
* [td_xmau_metrics_instance_adoption: Issues by ping_product_tier](https://app.periscopedata.com/app/gitlab/793297/xMAU-Analysis-Workflow---Example-Queries-and-Visualisations?widget=10776817&udv=0): `[td_xmau_metrics_instance_adoption(ping_product_tier, 'usage_activity_by_stage_monthly.plan.issues')]`

### Top 50 Instances Last Month: [td_xmau_metrics_top_users(metrics_path)]

The `[td_xmau_metrics_top_users]` snippet returns a list of the Top 50 Self-Managed instances 
using your specific feature last month. 

**Usage**

The `[td_xmau_metrics_top_users]` snippet requires 1 parameter: `metrics_path`.

1. **metrics_path:** the Service Ping metric of interest
  * You can find the metrics_path value in the [Service Ping metrics dictionary](https://metrics.gitlab.com/)

**Example**

* [Top 50 Users - MAU triggering deployments](https://app.periscopedata.com/app/gitlab/793297/xMAU-Analysis-Workflow---Example-Queries-and-Visualisations?widget=10768540&udv=0): `[td_xmau_metrics_top_users('usage_activity_by_stage_monthly.release.deployments')]`

### Instances with biggest MOM usage increase: [td_xmau_metrics_increasing_usage_users(metrics_path)]

The snippet renders a list of Top 50 Self-Managed Instances who have seen their monthly usage grow 
the most in the last completed month.

**Usage**

The `[td_xmau_metrics_increasing_usage_users]` snippet requires 1 parameter: `metrics_path`.

1. **metrics_path:** the Service Ping metric of interest
  * You can find the metrics_path value in the [Service Ping metrics dictionary](https://metrics.gitlab.com/)

**Example**

* [Top 50 Users with increasing usage - MAU triggering deployments](https://app.periscopedata.com/app/gitlab/793297/xMAU-Analysis-Workflow---Example-Queries-and-Visualisations?widget=10741403&udv=0): `[td_xmau_metrics_increasing_usage_users('usage_activity_by_stage_monthly.release.deployments')]`

### Instances with biggest MOM usage decrease: [td_xmau_metrics_decreasing_usage_users(metrics_path)]

The `[td_xmau_metrics_decreasing_usage_users]` snippet returns a list of Top 50 Self-Managed 
instances who have seen their monthly usage decrease the most in the last completed month. 

**Usage**

The `[td_xmau_metrics_decreasing_usage_users]` snippet requires 1 parameter: `metrics_path`.

1. **metrics_path:** the Service Ping metric of interest
  * You can find the metrics_path value in the [Service Ping metrics dictionary](https://metrics.gitlab.com/)

**Example**

* [Top 50 Users with decreasing usage - MAU triggering deployments](https://app.periscopedata.com/app/gitlab/793297/xMAU-Analysis-Workflow---Example-Queries-and-Visualisations?widget=10768538&udv=0): `[td_xmau_metrics_decreasing_usage_users('usage_activity_by_stage_monthly.release.deployments')]`

### Monthly change breakdown: [td_xmau_monthly_change_breakdown(metrics_path)]

The `[td_xmau_monthly_change_breakdown]` snippet enables you to better understand the 
growth/decrease of a specific monthly metric value by breaking usage into 4 buckets:

1. Decreased Usage: Comparing Month M with M-1, lower usage of a feature (for example, less users opening an issue)
1. Increased Usage: Comparing Month M with M-1, higher usage of a feature (for example, more users opening an issue)
1. New tracked instance: We haven't seen this instance reporting this feature counter before. That means either it is a new instance, or it just upgraded from an older version that didn't have the counter
1. No data received: An instance stopped reporting (it could be because of expiring subscription, churn, or disabling of service ping)

**Usage**

The `[td_xmau_monthly_change_breakdown]` snippet requires 1 parameter: `metrics_path`.

1. **metrics_path:** the Service Ping metric of interest
  * You can find the metrics_path value in the [Service Ping metrics dictionary](https://metrics.gitlab.com/)

**Example**

[Monthly breakdown change - Issues](https://app.periscopedata.com/app/gitlab/793297/xMAU-Analysis-Workflow---Example-Queries-and-Visualisations?widget=10768920&udv=0): `[td_xmau_monthly_change_breakdown('usage_activity_by_stage_monthly.plan.issues')]`

## Other snippets, tips, and tricks

### How to show dynamic targets from PI yaml files

The following snippets support adding dynamic target values from performance indicator yaml files:

* `td_xmau`
* `td_xmau_metrics_recorded_metric_value`
* `td_xmau_metrics_recorded_metric_value_with_total`
* `td_xmau_metrics_original_metric_value`

**Usage**

Adding the `target_name`, `monthly_recorded_targets`, `monthly_estimated_targets` keys to a 
performance indicator yaml file, shows the provided target values for the provided metric name.

* `target_name`: The target name should equal to the `metrics_path` used in the snippet
* `monthly_estimated_targets`: is a list of `"key": value` pairs where `key` is the target date 
to reach the target `value`
* `monthly_recorded_targets`: is a list of `"key": value` pairs where `key` is the target date 
to reach the target `value`

**Example**

Having the following code snippet in a performance indicator yaml:

```yaml
...
- name: Configure:Configure - SMAU, GMAU - MAU of GitLab Managed Terraform State
  target_name: counts.projects_with_terraform_states
  monthly_recorded_targets:
    "2021-04-20": 5000
    "2020-11-01": 3000
    "2020-07-01": 700
  monthly_estimated_targets:
    "2021-04-20": 6000
    "2020-11-01": 3000
    "2020-07-01": 700
...
```

### Forecasted xMAU

You can leverage the `[td_xmau]` snippet to forecast your PI growth over time. Check out 
[this video](https://www.youtube.com/watch?v=NmLb8eI6gtY) on how to get started.

* Edit the chart in SiSense.
* Below the query, expand the dropdown next to Python 3.7 and select `Predictive Forecast Plot`. 
* Next you'll need to edit the snippet to update the names of a couple of fields. You can 
reference the [query used in this chart to forecast Package stage GMAU](https://app.periscopedata.com/app/gitlab/805350/Package:-User-Adoption-and-Growth?widget=12924753&udv=0) 
(also displayed below)
* Now you are ready to click `Run SQL`
* Then scroll down and click `Run Python`
* Save the chart.
* Happy forecasting!

``` sql
WITH forecast AS (
  
  [td_xmau('All', 'GMAU', 'package', 0.1, 'metric/version check - subscription based estimation')]

)

SELECT  
  reporting_month AS ds_month,
  SUM(mau_value) AS y_value
FROM forecast
WHERE mau_value > 0 
  AND stage_name = 'All'
GROUP BY 1
 ```

<!--- TO DO - update this section once these snippets are refactored

##### `feature_usage_this_agg_period_and_growth` Snippet
The snippet called `feature_usage_this_agg_period_and_growth(feature)` ([SQL source](https://app.periscopedata.com/app/gitlab/snippet/feature_usage_this_agg_period_and_growth/5564f7aad54d4044834b48167e6befd8/edit)) is mainly used for the **old all-time counters** that have been implemented until 2019. Here is a list of the different fields that the snippet produces:

- feature_name: name of the pings you want to track. To get the exact name of the pings, please refer to [this model](https://dbt.gitlabdata.com/#!/model/model.gitlab_snowflake.version_usage_data_unpacked).
- feature_usage_sum: Total count for the specific feature.
- feature_usage_sum_change: count for the specific feature at a give period P.
- instance_count: Total count of instances that have been using this specific feature at least once before a given period P. In technical terms, it counts all instances which have a `feature_usage_sum` greater than 0 for this period.
- instance_count_change: count of instances that have been using this specific feature at a given period P. In technical terms, it counts all instances which have a `feature_usage_sum_change` greater than 0 for this period.
- average_per_12: average `feature_usage_sum` value over the previous 12 months.
- percent_growth:  POP growth.

This snippet can be used with the date-range and aggregation filters. It has been used in several dashboards, one of them is the [Secure Metrics dashboard](https://app.periscopedata.com/app/gitlab/410654/Secure-Metrics)

We are planning to build a second snippet to report on the adoption rate per [product tier](/handbook/marketing/strategic-marketing/tiers/). This snippet will allow to easily calculate among the instances that send us service pings in a given period, the number of instances that use a specific feature.

###### Examples

- **Example 1**: [What percent of self-managed instances sending service pings have a project with an active Jira integration?](https://app.periscopedata.com/app/gitlab/602123/Data-For-Product-Manager:-Supporting-doc?widget=7886001)
- **Example 2**: [What are the salesforce accounts of the 100 instances that use issues the most?](https://app.periscopedata.com/app/gitlab/602123/Data-For-Product-Managers:-Supporting-Doc?widget=7932958)
- **Example 3**: [How many SAST jobs have been created in the last 12 months by self-managed instances sending service pings ?](https://app.periscopedata.com/app/gitlab/602123/Data-For-Product-Manager:-Supporting-doc?widget=7886026)

##### usage_ping_metric_count Snippet

The snippet called `usage_ping_metric_count(feature)` ([SQL source](https://app.periscopedata.com/app/gitlab/snippet/usage_pings_metrics_count/559698d5a4294ef2b2fef94a4f777d96/edit)) is mainly used for the **28-days counters** that have been implemented from 2019.

You need only one parameter to start using this snippet, the feature name (which is the last part in the feature path). For example, for the redis_hll_counters data:

```json

{
    "analytics_unique_visits":{
        "analytics_unique_visits_for_any_target":0,
        "analytics_unique_visits_for_any_target_monthly":0,
        "g_analytics_contribution":0,
        "g_analytics_insights":0,
        "g_analytics_issues":0,
        "g_analytics_productivity":0,
        "g_analytics_valuestream":0,
        "i_analytics_cohorts":0,
        "i_analytics_dev_ops_score":0,
        "p_analytics_code_reviews":0,
        "p_analytics_insights":0,
        "p_analytics_issues":0,
        "p_analytics_pipelines":0,
        "p_analytics_repo":0,
        "p_analytics_valuestream":0
     }
}

```

If you want to create a very simple month-over-month chart of one specific metric like `analytics_unique_visits_for_any_target`, you will have to write:

```
[usage_ping_metric_count('analytics_unique_visits_for_any_target')]
```
--->

## Go further

These key snippets will help you generate some quick charts and key data about your metrics. Of 
course, this list of snippet is just a glimpse of what can be done with our current data models.

### Feedback

If you have ideas for new snippets that can enable data exploration or any other feedback about 
these snippets or this handbook page, please contribute to [this issue (internal link)](https://gitlab.com/gitlab-data/analytics/-/issues/7738) .
